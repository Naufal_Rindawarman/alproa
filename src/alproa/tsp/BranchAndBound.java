/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alproa.tsp;

import static alproa.tsp.UtilityClass.getArrayIndex;

public class BranchAndBound {
    public Cities cities;
    
     
    private City startCity;
    private City cityArray[];
    private City bestRoute[];   
    private int cityCount;
    private int solutionFoundCount = 0;
    private boolean stop = false;
    private float minDistance = -99;
           
    public BranchAndBound(Cities c){
        cities = c;
        cityCount = c.citylist.size();
        cityArray = new City[cityCount];
        c.citylist.toArray(cityArray);        
     }
    
    public void run(){
        float costMatrix[][] = new float[cityCount][cityCount];            
               
        City v[] = new City[1];
        startCity = cities.citylist.get(0);        
        v[0] = startCity;
        float cost = 0;
        
        //Buat cost matrix
        for (int i = 0; i<cityCount; i++){
            City a = cities.citylist.get(i);
            for (int j = 0; j<cityCount; j++){
                if (i == j){
                    //Jika kota asal dan tujuan sama, assign nilai -99
                    costMatrix[i][j] = -99; 
                } else {
                    City b = cities.citylist.get(j); 
                    costMatrix[i][j] =  a.getDistance(b);
                }                
            } 
        }  
        
        //Reduksi cost matrix, hitung cost pada kota asal
        cost += reduceRow(costMatrix, costMatrix);        
        cost += reduceColumn(costMatrix, costMatrix);
             
        long start = System.currentTimeMillis();
        traverse(startCity, v, cost, costMatrix);
        System.out.println("Branch and Bound duration: " 
                + (System.currentTimeMillis()-start));        
        
        //Print solusi
        System.out.println("Solution found: " + solutionFoundCount);
        System.out.println("Best solution: ");
           for (City ci : bestRoute){
               System.out.print(ci.name + " > ");
           }
        System.out.print(", Distance: " + minDistance);
        System.out.println();
    }
    
    //Setiap kota
    private void traverse(City c, City visited[], float parentcost, 
            float[][] reducedCostMatrix){        
        City neighbor[] = cityArray;
        float min = -99;
        int thisIdx = getArrayIndex(neighbor, c);
        float minReducedCostMatrix[][][] = 
                new float[cityCount][cityCount][cityCount];
        City nextCity[] = new City[cityCount];
        float nextCityCost[] = new float[cityCount];
        int child = 0;
        for (int i = 0; i< neighbor.length; i++){
            if (getArrayIndex(visited, neighbor[i]) >= 0){
                //Jika pernah dikunjungi, lewati ke kota tetangga berikutnya
                continue;                 
            }
            
            float cost = 0;
            
            float[][] parentCostMatrix = new float[cityCount][cityCount];
            
            for (int idx = 0; idx<reducedCostMatrix.length; idx++){
                parentCostMatrix[idx] = reducedCostMatrix[idx].clone();
            
            }
            //Cost kota ini = cost kota sebelumnya
            cost += reducedCostMatrix[thisIdx][i];
            
            setCostMatrixToInfinity(parentCostMatrix, thisIdx, i);
            
            //Hitung cost dengan mereduksi cost matrix
            cost += reduceRow(parentCostMatrix, parentCostMatrix);           
            cost += reduceColumn(parentCostMatrix, parentCostMatrix);
           
            cost += parentcost;            
            
            nextCityCost[child] = cost;
            nextCity[child] = neighbor[i];
            minReducedCostMatrix[child] = parentCostMatrix;  
            
            //Catat cost minimum
            if (min < 0 || cost < min){
                    min = cost;
            }              
            child++;
        }
        
        if (child == 0){
            //Jika tidak ada kota yang bisa dikunjungi
            //Kembali ke kota asal
            City v[] = new City[visited.length+1];
            int j;
            for (j = 0; j<visited.length; j++){
                v[j]= visited[j];
            }
            v[j] = startCity;           
            
            //Catat rute terbaik dengan jarak minimum         
            if (minDistance < 0 || parentcost < minDistance){
                bestRoute = v;
                minDistance = parentcost;    
            }
            solutionFoundCount++;           
        } else {
            City v[] = new City[visited.length+1];
            int j;
            for (j = 0; j<visited.length; j++){
                v[j]= visited[j];
            }
            
            for (int i = 0; i<nextCityCost.length; i++){
                //Batasi kota yang memiliki cost < min*1.1
                //Kota yang costnya lebih besar tidak akan ditelusuri
                if (nextCityCost[i] < min*1.1 && nextCityCost[i]> 0){
                    v[j] = nextCity[i];
                    traverse(nextCity[i], v, nextCityCost[i], 
                            minReducedCostMatrix[i]);
                }             
            }            
        } 
    }
    
    public void setCostMatrixToInfinity(float[][] matrix, int x, int y){
        for (int col = 0; col<cityCount; col++){
            matrix[x] [col] = -99;
        }
        for (int row = 0; row<cityCount; row++){
            matrix[row] [y] = -99;
        }
        matrix[y][x] = -99;
    }
    
    //Reduksi kolom matriks, kembalikan jumlah minimum setiap kolom
    public float reduceColumn(float arr[][], float returnArray[][]){
        float cost = 0;
        for (int col = 0; col<cityCount; col++){
            float min= -1;
            for (int row = 0; row<cityCount; row++){
                float val = arr[row][col];
                if (min < 0 || val < min && val >= 0){
                    min = val;
                }
            } 
            if (min >= 0){
                cost += min;
                //System.out.print(min + " + ");
                for (int row = 0; row<cityCount; row++){
                    if (arr[row][col]>0){
                        arr[row][col] -= min;                        
                    }                                      
                }
            }
        }        
        return cost;
    }
    
    //Reduksi baris matriks, kembalikan jumlah minimum setiap baris
    public float reduceRow(float arr[][], float returnArray[][]){
        float cost = 0;
        for (int row = 0; row<cityCount; row++){
            float min= -1;
            for (int col = 0; col<cityCount; col++){
                float val = arr[row][col];
                if (min < 0 || val < min && val >= 0){
                    min = val;
                }
            } 
            if (min >= 0){
                cost += min;
                
                for (int col = 0; col<cityCount; col++){
                    if (arr[row][col]>0){
                        arr[row][col] -= min;                        
                    }  
                } 
            }         
        }        
        return cost;
    }    
   
    //Utility procedure
    public void printArray(float[][] costMatrix){
        System.out.println("Reduced cost matrix"); 
        for (int i = 0; i<cityCount; i++){
            for (int j = 0; j<cityCount; j++){               
               System.out.print(costMatrix[i][j] + "\t\t");                            
            } 
            System.out.println(); 
        }
    }   
}
