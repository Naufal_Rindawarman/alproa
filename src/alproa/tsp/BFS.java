/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alproa.tsp;

import static alproa.tsp.UtilityClass.*;

public class BFS {
    public Cities cities;
    public float minDistance;   
    public City bestRoute[];
    private City cityArray[];
    private City startCity;
        
    public BFS(Cities c){
        minDistance = 0;
        cities = c;
    }
    
    public void run(){
       startCity = cities.citylist.get(0);
       cityArray = new City[cities.citylist.size()];
       cities.citylist.toArray(cityArray);
       City visited[][] = new City[1][1];
       City start[] = new City[1];
       visited[0][0] = startCity;
       start[0] = startCity;
       long startTime = System.currentTimeMillis();
       float d[] =  new float[1];
       d[0] = 0;
       traverse (start, visited, d);
       System.out.println("BFS duration: " + 
               (System.currentTimeMillis()-startTime));
       
       //Print solusi
       if (minDistance > 0){
           System.out.print("Optimal route: ");
           for (City ci : bestRoute){
                System.out.print(ci.name + " > ");
           }
           System.out.println("Distance: " + minDistance);
       } else {
           System.out.println("No solution");
       }       
       
    }  
    
    private void traverse(City carr[], City visited[][], 
            float parentDistance[]){
        int child = 0;
        City level[] = 
                new City[(cityArray.length-visited[0].length)*carr.length];
        City visitedArray[][] = 
                new City[carr.length*(cityArray.length-visited[0].length)][];
        float distances[] = 
                new float[(cityArray.length-visited[0].length)*carr.length];
        
        //Untuk setiap kota pada level pohon BFS
        for (int idx = 0; idx < carr.length; idx++){    
            int thisChild = 0;
            for (City c2 : cityArray){   
                //Jika kota ini belum dikunjungi
                if (!isArrayContain(visited[idx], c2)){
                    int i;             
                    City v[] = addCityArray(visited[idx], c2);
                    
                    //Masukkan untuk level berikutnya
                    level[child] = c2;
                    visitedArray[child] = v ;
                    distances[child] = parentDistance[idx];
                    distances[child] += carr[idx].getDistance(c2);

                    child++;
                    thisChild++;
                }
            }
            
            //Kota ini tidak ada yang bisa dikunjungi lagi            
            if (thisChild == 0){
                if (true){
                    City v[];;                
                    v = addCityArray(visited[idx], startCity);
                    float d = parentDistance[idx];
                    d += startCity.getDistance(carr[idx]);
                    if (minDistance == 0 || d < minDistance){
                            minDistance = d;
                            bestRoute = v;
                    }
                }
            }             
        }
        
        //Jika kota di suatu level pada pohon memiliki kota yang bisa 
        //dikunjungi, telusuri level di bawahnya
        if (child > 0){
            traverse(level, visitedArray, distances);
        }
    }
}
