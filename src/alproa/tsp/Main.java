/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alproa.tsp;

public class Main {
    static Cities c;
   
    public static void main(String[] args) {
       
        //Ganti jumlah kota
        int numberOfCities = 5;        
        
        c = new Cities(numberOfCities);
        
        runGreedy();
        runBruteForce();
        runDFS();
        runBFS();
        runBranchAndBound();
        runBacktrack();        
    }
    
    public static void runGreedy(){
        System.out.println("Greedy Start");
        Greedy gr = new Greedy(c);
        gr.run();
        System.out.println("Greedy End\n");
    }
    
    public static void runBruteForce(){
        System.out.println("BruteForce Start");
        BruteForce bf = new BruteForce(c);
        bf.run();
        System.out.println("BruteForce End\n");
    }
    
    public static void runBacktrack(){
        System.out.println("Backtrack Start");
        Backtrack bt = new Backtrack(c);
        bt.run();
        System.out.println("Backtrack End\n");
    }
    
    public static void runBranchAndBound(){
        System.out.println("Branch and Bound Start");
        BranchAndBound bnb = new BranchAndBound(c);
        bnb.run();
        System.out.println("Branch and Bound End\n");
    }
    
    public static void runDFS(){
        System.out.println("DFS Start");
        DFS dfs = new DFS(c);
        dfs.run();        
        System.out.println("DFS End\n");
    }
    
    public static void runBFS(){
        System.out.println("BFS Start");
        BFS bfs = new BFS(c);
        bfs.run();
        System.out.println("BFS End\n");
    }    
}
