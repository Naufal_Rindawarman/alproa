/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alproa.tsp;

/**
 *
 * @author NDSR
 */
public class Point {
    int x;
    int y;
    
    public Point(int a, int b){
        x = a;
        y = b;
    }
    
    public int getX(){
        return x;
    }
    
    public int getY(){
        return y;
    }
    
    public static float getDistance(Point p1, Point p2){
        float d;
        d = (float) Math.sqrt(Math.pow(p1.x-p2.x, 2) + Math.pow(p1.y-p2.y, 2));
        return d;
    }
}
