/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alproa.tsp;

public class City {
    public String name;
    public Point point;
    
    public City (String s, Point p){
        name = s;
        point = p;
    }
    
    public City (String s, int x, int y){
        name = s;
        point = new  Point(x,y);
    }
    
    public float getDistance(City c){
        float d;
        d = Point.getDistance(point, c.point);
        return d;
    }
    
    public static float getTwoCityDistance (City c1, City c2){
        float d;
        d = Point.getDistance(c1.point, c2.point);
        return d;
    }
}
