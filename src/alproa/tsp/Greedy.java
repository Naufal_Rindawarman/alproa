/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alproa.tsp;

import static alproa.tsp.UtilityClass.getArrayIndex;

public class Greedy {    
    public Cities cities;
    private City startCity;
    private City cityArray[];    
  
    public Greedy(Cities c){
        cities = c;
     }
    
    public void run(){       
       startCity = cities.citylist.get(0);
       cityArray = new City[cities.citylist.size()];
       cities.citylist.toArray(cityArray);
       City visited[] = new City[1];
       visited[0] = startCity;
       
       long start = System.currentTimeMillis();
       traverse(startCity, visited,  0);
       System.out.println("Greedy duration: " + 
               (System.currentTimeMillis()-start));
       
    }
    
    private void traverse(City c, City[] visited, float distance){        
        float localDistance = 0;
        float localDistanceMin = -99;
        City next = null;
        //Untuk setiap kota yang bertetangga
        for (City c2 : cityArray){
            if (getArrayIndex(visited,c2) < 0){
                //Jika kota belum dikunjungi
                localDistance = c.getDistance(c2);
                //Simpan kota tetangga yang paling dekat
                if (localDistanceMin < 0 || localDistance < localDistanceMin){
                    next = c2;                    
                    localDistanceMin = localDistance;                    
                }
            }
        }
        
        City v[] = new City[visited.length+1];
        int j;
        for (j = 0; j<visited.length; j++){
            v[j]= visited[j];
        }
        
        float d = distance;
        
        if (next != null){
            //Jika ada kota tetangga yang bisa dikunjungi
            //Telusuri kota tetangga yang paling dekat
            d+= localDistanceMin;
            v[j] = next;
            traverse(next, v,  d);
        } else {
            //Jika tidak ada kota tetangga yang bisa dikunjungi
            //Kembali ke kota asal, hitung jarak kota terakhir dengan kota asal
            v[j] = startCity;
            d+= c.getDistance(startCity);
            
            //Print solusi
            for (City ci : v){
                System.out.print(ci.name + " > ");
            }
            System.out.print("Distance: " + d);
            System.out.println();
        }
    }    
}
