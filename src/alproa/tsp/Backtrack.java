/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alproa.tsp;

import static alproa.tsp.UtilityClass.*;

public class Backtrack {
    public Cities cities;
    private float minDistance = -99;
    private City minRoute[];
    private City startCity;
    private City cityArray[];    

    public Backtrack(Cities c){
        cities = c;
    }
    
    public void run(){
        City visited[] = new City[1];
        startCity = cities.citylist.get(0);
        visited[0] = startCity;
        cityArray = new City[cities.citylist.size()];
        cities.citylist.toArray(cityArray);

        long start = System.currentTimeMillis();
        traverse (cities.citylist.get(0), visited, 0 );
        System.out.println("Backtrack duration: " 
                + (System.currentTimeMillis()-start));
        
        //Print solusi
        System.out.println("Solution");
        for (City ci : minRoute){
            System.out.print(ci.name + " > ");
        }
        System.out.print(", Distance : " + minDistance);
        System.out.println();
        
    }
    
    //Telusuri setiap kota
    public void traverse(City c, City visited[], float distance){
        //Jika jarak sampai saat ini kurang dari jarak solusi terbaik saat ini
        //Lanjutkan penelusuran
        if (distance < minDistance || minDistance < 0){
            int neighborCount  = 0;
            //Untuk setiap kota yang bertetangga
            for (City neighbor: cityArray){
                //Jika kota pernah dikunjungi, lewati
                if (getArrayIndex(visited, neighbor) >= 0){
                    continue;
                }
                float d = distance;
                d += c.getDistance(neighbor);
                City v[] = addCityArray(visited, neighbor);
                traverse(neighbor, v, d);
                neighborCount++;
            }
            
            //Jika tidak ada kota tetangga yang bisa dikunjungi
            if (neighborCount == 0){             
                //Kembali ke kota asal
                float d = distance;
                d += c.getDistance(startCity);
                City v[] = addCityArray(visited, startCity);
                
                //Catat rute terbaik dan jaraknya
                if (minDistance < 0 || d < minDistance){                   
                    minDistance = d;
                    minRoute = v;
                }
            }
        }        
    }  
}


