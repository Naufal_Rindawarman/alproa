/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alproa.tsp;

import java.util.ArrayList;
import java.util.Iterator;

//Kelas daftar kota dan hubungannya
public class Cities {
    public ArrayList<City> citylist;
    public Neighbors neighbors;
 
    //Tambah daftar kota
    public Cities(){
        citylist = new ArrayList<City>();
        
        citylist.add(new City("A", 1, 1));
        citylist.add(new City("B", 30,100));
        citylist.add(new City("C", 10, 15));
        citylist.add(new City("D", 12, 34));
        citylist.add(new City("E", 20, 5));
        citylist.add(new City("F", 2, 4));
        citylist.add(new City("G", 16, 40));
        //initNeighbor();
        //citylist.add(new City("A", 0, 12));
        //citylist.add(new City("B", 0, 400));
        //citylist.add(new City("C", 0, 42));
        //citylist.add(new City("D", 0, 90));
        initNeighborAll();
    }
    
    public Cities(int n){
        int l = n*10;
        if (n>0){
            citylist = new ArrayList<City>();
            for (int i = 0; i<n; i++){
                citylist.add(new City("City " + String.valueOf(i), 
                        Math.round((float) (Math.random()*l)), 
                        Math.round((float) (Math.random()*l))));
            }
            initNeighborAll();
        }
        
    }
    
    //Tambah daftar ketetanggaan kota
    public void initNeighbor(){
        neighbors = new Neighbors();
        addNeighbor("A","B");
        addNeighbor("A","C");
        addNeighbor("A","F");
        addNeighbor("B","C");
        addNeighbor("B","D");
        //addNeighbor("C","F");
        addNeighbor("C","D");
        addNeighbor("D","E");
        addNeighbor("D","F"); 
        addNeighbor("E","F");
        addNeighbor("E","G");
        addNeighbor("G","F"); 
    }
    
    //Membuat semua kota menjadi terhubung
    public void initNeighborAll(){
        neighbors = new Neighbors();
        for (City c1 : citylist){
            for (City c2 : citylist){
                if (!c1.equals(c2)){
                    neighbors.addNeighbor(new Neighbor(c1,c2));
                }
            }
        }
    }
    
    //Menghubungkan dua buah kota
    public void addNeighbor(String a, String b){
        Iterator<City> it1 = citylist.iterator();
        City c1 = null;
        City c2 = null;
        while (it1.hasNext() && (c1 == null || c2 == null)){
            City c = it1.next();
            if (c.name == a && c1 == null){
                c1 = c;
            } else if (c.name == b && c2 == null) {
                c2 = c;
            }
        }
        if (c1 != null && c2 != null){
            neighbors.addNeighbor(new Neighbor(c1,c2));
        }
    }   
    
    //Print semua daftar kota dan hubungannya
    public void printCitiesAndNeighbors(){
        for (City c : citylist){
            System.out.println("City " + c.name + ", location : " + c.point.x + "," + c.point.y);
        }
        
        for (Neighbor n: neighbors.neighborlist){
            System.out.println("City " + n.city1.name + " neighboring " + n.city2.name + ", distance " + n.city1.getDistance(n.city2));
        }
    }
    
    public void printNeighbors(City c){
        System.out.println("City " + c.name + " neighbors with ");
        for (Neighbor n: neighbors.neighborlist){
            City nc = n.getNeighbor(c);
            if (nc != null) {
                System.out.println(nc.name);
            }
        }      
    }
    
    //Return semua kota yang bertetangga dengan Kota K
    public ArrayList<City> getAllNeighbors(City c){
        ArrayList<City> l = new ArrayList<City>();
        for (Neighbor n: neighbors.neighborlist){
            City nc = n.getNeighbor(c);
            if (nc != null) {
                l.add(nc);
            }
        }            
        return l;
    }
    
    
    public City[] getAllNeighborsArray(City c){
        City arr[] = new City[citylist.size()-1];
        int i = 0;
        for (Neighbor n: neighbors.neighborlist){
            City nc = n.getNeighbor(c);
            if (nc != null) {
                arr[i] = nc;
                i++;
            }            
        }
        return arr;
    }
    
    public City getNeighbor(City c, int i){
        City r = null;
        int ntimes = 0;
        for (Neighbor n: neighbors.neighborlist){
            City nc = n.getNeighbor(c);
            if (nc != null) {
                if (i == ntimes){
                    r = nc;
                    break;
                } else {
                    ntimes++;
                }
                
            }
        }            
        return r;
    }    
}
