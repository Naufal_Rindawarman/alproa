/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alproa.tsp;

/**
 *
 * @author NDSR
 */
public class UtilityClass{
    
     public static int getArrayIndex (City arr[], City c){
        int idx = -1;
        int i = 0;
        for (City c1 : arr){
            if (c1 == c){
                idx = i;
                break;
            }
            i++;
        }
        return idx;
    }
     
    
     
    public static City[] addCityArray (City arr[], City c){
        return addCityArray (arr, c, arr.length);    
    }
    
    public static City[] addCityArray (City arr[], City c, int insertIdx){
        City newArray[] = new City[arr.length+1];
        int offset = 0;
        for (int i = 0; i < newArray.length; i++){
            if (i == insertIdx){
                newArray[i] = c;
                offset+=1;
            } else {
                newArray[i] = arr[i-offset];
                //newArray[i] = c;
            }
        }
        return newArray;
     }
    
    public static boolean isArrayContain(City arr[], City c){
        boolean found = false;
        for (City c1 : arr){
            if (c1 == c){
                found = true;
                break;
            }
        }
        return found;
    }
    
}
