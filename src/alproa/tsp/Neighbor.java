/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alproa.tsp;

public class Neighbor {
    public City city1;
    public City city2;

    public Neighbor(City c1, City c2){
        city1 = c1;
        city2 = c2;
    }

    //Mendapatkan kota pasangan
    public City getNeighbor(City c){
        if (c.equals(city1)){
            return city2;
        } else if (c.equals(city2)){
            return city1;
        } else {
            return null;
        }
    }
}
