/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alproa.tsp;

import java.util.ArrayList;

/**
 *
 * @author NDSR
 */
public class BranchAndBound2 {
    public Cities cities;
    public boolean stop = false;
    
    public BranchAndBound2(){
        cities = new Cities();
     }
    
    public void traverse(){
       ArrayList<City> visited = new ArrayList<City>();
       visited.add(cities.citylist.get(0));
       traverse (cities.citylist.get(0), visited, 0 );
    }
    
    private void traverse(City c, ArrayList<City> visited, float distance){
        City end = c;
        ArrayList<City> ar = cities.getAllNeighbors(c);  
        float total_distance = distance;
        int idx = 0;
        
        if (ar.size()<2){
                System.out.println("Ditemukan kota " + c.name + " yang hanya memiliki satu kota tetangga. Tidak ada solusi");
                stop = true;
                return;
        }
        
        //Hitung cost, urutkan cost berdasarkan paling kecil
        int lastsortidx = 0;
        int idx2 = 0;
        float d2;
        float d3;
        
        /*
        System.out.print("Unsorted: ");
        for (City c3 : ar){
            System.out.print(c3.name + "(" + c3.getDistance(c) + ") > ");
        }
        System.out.println();
        */
        
        //Sort berdasarkan cost
        while (lastsortidx < ar.size()){
            City c2 = ar.get(idx2);
            d2 = c.getDistance(c2);
            int moveidx = lastsortidx;
            while (idx2>0){
                City c3 = ar.get(idx2-1);
                d3 = c.getDistance(c3);                
                if (d2<d3){
                    moveidx = idx2-1;
                    //System.out.println(c2.name + " Smaller than " + c3.name );
                }
                idx2--;
            }
            City ctemp = ar.remove(lastsortidx);
            ar.add(moveidx, ctemp );
            lastsortidx++; 
            idx2 = lastsortidx;
        }
        
        /*
        System.out.print("Sorted: ");
        for (City c3 : ar){
            System.out.print(c3.name + " > ");
        }
        System.out.println();
        */
        
        for (City c2 : ar){
            if (stop){
                break;
            }
            total_distance = distance;
            ArrayList<City> v = (ArrayList<City>) visited.clone();   
            if (!v.contains(c2)){
                v.add(c2);
                idx++;
                total_distance += c.getDistance(c2);
                traverse(c2, v, total_distance);
                //System.out.print(c3);
            }
        }
        
        /*
        //Jika tidak ada jalan lain
        if (idx == 0){ 
            String solusi = "Path not found";
            if (visited.containsAll(cities.cities)){                
                if (ar.contains(cities.cities.get(0))){
                    visited.add(cities.cities.get(0));
                    d += cities.cities.get(0).getDistance(c);
                    solusi = "Path found";
                }
            }
            for (City ci : visited){
                System.out.print(ci.name + " > ");
            }
            System.out.print(d + ", " + solusi);
            System.out.println();
        }
                */
        
        //Jika tidak ada jalan lain
        if (idx == 0){   
            String str = "Path not found";
            if (visited.containsAll(cities.citylist)){
                 if (ar.contains(cities.citylist.get(0))){
                    stop = true;
                    visited.add(cities.citylist.get(0));
                    total_distance += cities.citylist.get(0).getDistance(c);
                    str = "Path found";
                } else {
                }
            }
           
            for (City ci : visited){
                System.out.print(ci.name + " > ");
            }
            System.out.print(total_distance + ", " + str);
            System.out.println();
        }     
       
        
    }
}
