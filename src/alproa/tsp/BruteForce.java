/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alproa.tsp;

import java.util.ArrayList;

public class BruteForce {
    public Cities cities;
    public float minDistance;
    public ArrayList<City> bestRoute;
    public City startcity;

    public BruteForce(Cities c){
        minDistance = -99;
        cities = c;
     }   
    
    public void run(){
        startcity = cities.citylist.get(0);
        ArrayList<City> remainingCity = 
                (ArrayList<City>)cities.citylist.clone();
        remainingCity.remove(0);
        City c[] = new City[remainingCity.size()];
        remainingCity.toArray(c);
        
        long start = System.currentTimeMillis();
        //Lakukan exhaustive search dengan permutasi kota
        heapPermutation(c, c.length, c.length);
        System.out.println("Bruteforce duration: " + 
                (System.currentTimeMillis()-start));
        
        //Print solusi
        System.out.println("Solution:");
        for (City ci : bestRoute){
                System.out.print(ci.name + " > ");
           }
        System.out.print(", Distance = " + minDistance);
        System.out.println();
    }
    
    // Permutasi kota
    void heapPermutation(City a[], int size, int n) {        
        if (size == 1) hitungSolusi(a,n); 
        for (int i=0; i<size; i++){
            heapPermutation(a, size-1, n);
            
            if (size % 2 == 1){
                City temp = a[0];
                a[0] = a[size-1];
                a[size-1] = temp;
            } else {
                City temp = a[i];
                a[i] = a[size-1];
                a[size-1] = temp;
            }
        }
    }
    
    //Hitung solusi
    void hitungSolusi(City a[], int n){
        float d = 0;
        ArrayList<City> route = new ArrayList<>();
        route.add(startcity);
        //Hitung jarak rute
        for (int i=0; i<n; i++){
            if (i==0){
                d+= a[i].getDistance(startcity);
            }
            if (i == n-1){
                d+= a[i].getDistance(startcity);
            }
            if (!(i == n) && !(i==0)){
                d+= a[i].getDistance(a[i-1]);
            }
            route.add(a[i]);
        }
        route.add(startcity);
        
        //Masukkan ke solusi akhir jika lebih baik
        if (minDistance == -99 || d < minDistance){
           minDistance = d;
           bestRoute = route;
        }
       
    }
}
