/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alproa.tsp;

import java.util.ArrayList;

/**
 *
 * @author NDSR
 */

public class Neighbors {
    public ArrayList<Neighbor> neighborlist;
    
    public Neighbors(){
        neighborlist = new ArrayList<Neighbor>();
    }
    
    //Tambah pasangan kota A dan B ke array jika belum ada
    public boolean addNeighbor(Neighbor n2){
        boolean exist = false;
        if (n2.city1.equals(n2.city2)){
            return exist;
        }
        for (Neighbor n: neighborlist){
            City other = n.getNeighbor(n2.city1);
            if (other == n2.city2){
                return exist;
            }
        }
        neighborlist.add(n2);
        return true;
    }
}

