/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alproa.tsp;

import static alproa.tsp.UtilityClass.*;

public class DFS {
    public Cities cities;
    public float minDistance;
    public City bestRoute[];
    private City cityArray[];
    private City startCity;    
   
    public DFS(Cities c){
        minDistance = 0;
        cities = c;
    }
    
    public void run(){
       startCity = cities.citylist.get(0);
       cityArray = new City[cities.citylist.size()];
       cities.citylist.toArray(cityArray);
       City visited[] = new City[1];
       visited[0] = startCity;
       long start = System.currentTimeMillis();
       traverse (startCity, visited, 0 );
       System.out.println("DFS duration: " + 
               (System.currentTimeMillis()-start));       
       
       //Print solusi
       if (minDistance > 0){
           System.out.print("Optimal route: ");
           for (City ci : bestRoute){
                System.out.print(ci.name + " > ");
           }
           System.out.println("Distance: " + minDistance);
       } else {
           System.out.println("No solution");
       }       
       
    }    
                
    private void traverse(City c, City visited[], float distance){
        int child = 0;
        //Untuk setiap kota yang bertetangga
        for (City c2 : cityArray){                   
            float d = distance;
            //Jika kota tetangga c2 belum pernah dikunjungi
            if (!isArrayContain(visited, c2)){
                int i;             
                City v[] = addCityArray(visited, c2);                
                child++;
                d += c.getDistance(c2);
                //Telusuri kota tetangga c2
                traverse(c2, v, d);
            }
        }
        
        //Jika tidak ada kota tetangga yang bisa dikunjungi lagi
        if (child == 0){
            //Kembali ke kota asal
            City v[];              
            v = addCityArray(visited, startCity);
            distance += startCity.getDistance(c);
            if (minDistance == 0 || distance < minDistance){
                    minDistance = distance;
                    bestRoute = v;
            }           
        }
    } 
}

